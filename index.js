const mongoose = require("mongoose");
const User = require("./user");

const URI_MONGO = "mongodb+srv://everton:everton1@cluster0.bpzzr.mongodb.net/Everton-mongo?retryWrites=true&w=majority";

mongoose.connect(URI_MONGO, { useNewUrlParser: true });

 async function listAllUsers() {
    const users = await User.find();
    console.log(users)
}

function addUser() {
    const user = new User({
        name: "User 1"
    });

    return user.save()
}

function updateUser() {
    return User.updateOne({ name: "User 1" }, { name: "User 1.1" });
}

function deleteUser() {
    return User.deleteOne({ name: "User 1.1" });
}

async function initializeApplication() {
    await addUser();
    listAllUsers();
    await updateUser();
    console.log("Updated user below");
    listAllUsers();
    await deleteUser();
    console.log("User was deleted")
    listAllUsers();
}

initializeApplication();